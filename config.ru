Bundler.require(:default)
$LOAD_PATH.unshift(File.expand_path('../lib', __FILE__))
require 'middlewares/status'
require 'middlewares/webhook'
require 'middlewares/crash_lobster'
require 'rack/lobster'

Faye::WebSocket.load_adapter('puma')

# The default OpenShift websockets port is 8000
# Local default port of puma is 9292
ENV['WEBSOCKET_PORT'] = ENV['OPENSHIFT_RUBY_PORT'].nil? ? '9292' : '8000'

run Rack::URLMap.new(
  '/status'  => Middlewares::Status.new,
  '/webhook' => Middlewares::Webhook.new,
  '/'        => Middlewares::CrashLobster.new(Rack::Lobster.new),
)
