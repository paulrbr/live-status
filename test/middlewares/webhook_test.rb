require 'test_helper.rb'
require 'middlewares/webhook'

class WebhookTest < Test::Unit::TestCase
  include Rack::Test::Methods

  def app
    Middlewares::Webhook.new
  end

  def setup
    ENV['GITLAB_WEBHOOK_TOKEN'] = 'secret'
    header 'X-Gitlab-Token', ENV['GITLAB_WEBHOOK_TOKEN']
  end

  def test_simple
    post '/', {}
    assert last_response.ok?
    assert_equal 'Thanks', last_response.body
  end

  def teardown
    ENV.delete('GITLAB_WEBHOOK_TOKEN')
  end
end
