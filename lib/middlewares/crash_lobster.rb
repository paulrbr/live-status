require "rack/lobster"

module Middlewares
  class CrashLobster
    def initialize(app)
      @app = app
    end

    def call(env)
      @app.call(env)
    rescue RuntimeError
      # Lobster Crash ?flip=crash
      [200, { 'X-Hello' => 'Sorry, did not crash :)!'}, [crash_the_lobster]]
    end

    private

    def crash_the_lobster
      Rack::Lobster::LobsterString.split("\n").map do |line|
        line.chars.shuffle.join()
      end.join("\n")
    end
  end
end
