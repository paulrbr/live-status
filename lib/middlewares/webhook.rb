require 'middlewares/web_socket'
require 'gitlab/generic_serializer'

Thread.abort_on_exception = true

module Middlewares
  class Webhook
    def call(env)
      req = Rack::Request.new(env)
      params = params(req)

      if gitlab_trigger?(req)
        announce(params)

        [200, {}, ['Thanks']]
      else
        [404, {}, ['Nothing to see here']]
      end
    end

    private

    def params(request)
      raw = request.body.read
      if request.env['CONTENT_TYPE'] == 'application/json'
        Gitlab::GenericSerializer.new.deserialize(raw)
      else
        raw
      end
    end

    def announce(params)
      attributes = params['object_attributes']
      trigger_ref = attributes && attributes['ref']
      project = params['project']
      default_ref = project && project['default_branch']

      if trigger_ref == default_ref
        Middlewares::WebSocket.connected.each do |ws|
          ws.send(Gitlab::GenericSerializer.new(params).serialize)
        end
      end
    end

    def gitlab_trigger?(req)
      req.post? && req.env['HTTP_X_GITLAB_TOKEN'] &&
        req.env['HTTP_X_GITLAB_TOKEN'] == ENV['GITLAB_WEBHOOK_TOKEN']
    end
  end
end
