require 'faye/websocket'
require 'json'
require 'erb'

module Middlewares
  class WebSocket
    KEEPALIVE_TIME = 15 # in seconds

    def call(env)
      if Faye::WebSocket.websocket?(env)
        ws = Faye::WebSocket.new(env, nil, ping: KEEPALIVE_TIME)

        ws.on :open do |e|
          self.class.subscribe(ws)
          on_open(ws, e)
        end
        ws.on :message do |e|
          on_message(ws, e)
        end
        ws.on :close do |e|
          self.class.unsubscribe(ws)
          on_close(ws, e)
          ws = nil
        end

        # Return async Rack response
        ws.rack_response
      else
        [301, { 'Location' => '/'}, ["Nothing to see here"]]
      end
    rescue => e
      p e.inspect
      raise e if ENV['RACK_ENV'] == 'production'
    end

    @@connected_ws = []

    def self.connected
      @@connected_ws
    end

    def self.subscribe(ws)
      @@connected_ws << ws
    end

    def self.unsubscribe(ws)
      @@connected_ws.delete_if do |o|
        o.object_id == ws.object_id
      end
    end

    private

    def on_open(ws, event)
      p [:open]
    end

    def on_close(ws, event)
      p [:close]
    end

    def on_message(ws, event)
      p [:message, event.data]
    end

    def sanitize(message)
      json = JSON.parse(message)
      json.each {|key, value| json[key] = ERB::Util.html_escape(value) }
      JSON.generate(json)
    end
  end
end
