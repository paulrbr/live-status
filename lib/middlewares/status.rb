require 'middlewares/web_socket'
require 'gitlab/client'
require 'json'

Thread.abort_on_exception = true

module Middlewares
  class Status < WebSocket

    private

    def on_open(ws, _e)
      @thread = Thread.new{
        Gitlab::Client.if_running_build do |data|
          ws.send(data)
        end
      }
    end

    def on_message(ws, event)
      p [:message, event.data]

      if event.data == 'ping'
        ws.send(JSON.generate(data: 'pong'))
      end
    end

    def on_close(ws, _e)
    end

  end
end
