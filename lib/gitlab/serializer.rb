require 'json'

module Gitlab
  class Serializer

    attr_accessor :data

    def initialize(response = nil)
      return if response.nil?

      @data = expose(response)
    end

    def serialize
      JSON.dump(@data)
    end

    def deserialize(raw_response)
      @data = JSON.parse(raw_response)
    end

    private

    def expose(_model)
      fail NotImplementedError, 'Please define a Serialize#expose method depending of your data'
    end
  end
end
