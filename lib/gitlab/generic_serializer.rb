require 'gitlab/serializer'

module Gitlab
  class GenericSerializer < Serializer
    private

    def expose(data)
      data.to_h
    end
  end
end
