require 'api/client'
require 'gitlab/build_serializer'

module Gitlab
  module Default
    API_ENDPOINT = "https://gitlab.com/api".freeze
    API_VERSION  = "v4".freeze

    class << self

      # Include mandatory module
      include Api::DefaultOptions

      def api_endpoint
        API_ENDPOINT
      end

      def api_version
        API_VERSION
      end

    end
  end

  class Client < Api::Client
    # Include mandatory modules
    include Api::Configurable
    include Api::Connection

    PAULRBR_GITLAB_IO_PROJECT = 1271924.freeze
    GITLAB_PRIVATE_TOKEN = (ENV['GITLAB_TOKEN'] || '').freeze

    @instance = new

    def self.needs_update?(status)
      %w(running pending).include?(status)
    end

    def self.if_running_build
      last_running = last_builds(["running", "pending"]).first

      p last_running.inspect

      if last_running
        yield(Gitlab::BuildSerializer.new(last_running).serialize)
        last_running = nil
      end
    end

    def self.last_builds(scope = [])
      @instance.get(
        "/projects/#{PAULRBR_GITLAB_IO_PROJECT}/jobs",
        headers: {
          'PRIVATE-TOKEN' => "#{GITLAB_PRIVATE_TOKEN}"
        },
        query: {
          scope: scope
        }
      )
    end
  end
end
