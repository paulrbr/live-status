require 'gitlab/serializer'

module Gitlab
  class BuildSerializer < Serializer

    private

    def expose(build)
      {
        object_kind: "build",
        build_status: build.status,
        build_started_at: build.started_at,
        build_finished_at: build.finished_at
      }
    end
  end
end
