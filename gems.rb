source 'https://rubygems.org'

# Bundler for Openshift to be happy
gem 'bundler'
# App server
gem 'puma'

# App dependencies
gem 'rack'
gem 'faye-websocket', :require => 'faye/websocket'
gem 'apii', '~> 0.0.4', :require => 'api'

group :test do
  gem 'test-unit'
  gem 'rack-test'
end

group :development do
  gem 'pry'
end
