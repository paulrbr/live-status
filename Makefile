.PHONY: run
run: build
	docker run -d -p 127.0.0.1:9292:9292 --env-file=.env --restart=always -ti live-status

.PHONY: build
build:
	docker build -t live-status .
